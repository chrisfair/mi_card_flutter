import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        backgroundColor: Colors.lightBlueAccent,
        body: SafeArea(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              CircleAvatar(
                radius: 50.0,
                backgroundImage: AssetImage('images/avatar.png'),
              ),
              Text(
                'Christopher P. Fair',
                style: TextStyle(
                  fontSize: 30.0,
                  fontFamily: 'Cookie',
                ),
              ),
              Text(
                'Software Engineer',
                style: TextStyle(
                  fontSize: 20.0,
                  fontFamily: 'Play',
                  color: Colors.blue[900],
                  fontWeight: FontWeight.bold,
                ),
              ),
              SizedBox(
                height: 20.0,
                width: 150.0,
                child: Divider(color: Colors.black),
              ),
              Card(
                margin: EdgeInsets.symmetric(vertical: 10.0, horizontal: 25.0),
                child: ListTile(
                  leading: Icon(
                    Icons.phone,
                    size: 20,
                    color: Colors.blue[900],
                  ),
                  title: Text(
                    '1 (970) 402-3156',
                    style: TextStyle(
                      fontSize: 15.0,
                      fontFamily: 'Play',
                      color: Colors.blue[900],
                    ),
                  ),
                ),
              ),
              Card(
                margin: EdgeInsets.symmetric(vertical: 10.0, horizontal: 25.0),
                child: ListTile(
                  leading: Icon(
                    Icons.email,
                    size: 20,
                    color: Colors.blue[900],
                  ),
                  title: Text(
                    'christopherpatrickfair@gmail.com',
                    style: TextStyle(
                      fontSize: 15.0,
                      fontFamily: 'Play',
                      color: Colors.blue[900],
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
